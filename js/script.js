//Main Variables
let TheInput = document.querySelector('.get-repos input');

let getButn =  document.querySelector('.get-repos .get-button');

let reposData =  document.querySelector('.show-data');

getButn.onclick = function(){
    getRepos();
}

//Get Repos Function
function getRepos(){

    //console.log("function get Repos");

    if(TheInput.value == ""){

        //console.log("this value cant be empty");

        reposData.innerHTML = "<span>Please Write GitHube UserName</span>";

    }else{

        //console.log(TheInput.value);

        fetch(`https://api.github.com/users/${TheInput.value}/repos`).then((response)=>{
            return response.json();
        }).then((data) => {
            //console.log(data);

            //Empty the Container
            reposData.innerHTML = "";
            
            // Lop In Respositeries
            data.forEach(repo => {
                
                //Create Main Div Element
                let mainDive = document.createElement("div");

                //Create Repo Name Text
                let repoName = document.createTextNode(repo.name);

                //Append the Text To Main Div
                mainDive.appendChild(repoName);

                //create repo url 
                let theUrl = document.createElement('a');

                //create repo url text
                let theUrlText = document.createTextNode("visit");

                //Append the rrepo url Text to tage
                theUrl.appendChild(theUrlText);

                //add the href 
                theUrl.href = `https://github.com/${TheInput.value}/${repo.name}`;

                //Set Attr Blank 
                theUrl.setAttribute('target','_blank');

                //Append Url To Main Dive
                mainDive.appendChild(theUrl);

                //create stars count span
                let StarsSpan = document.createElement("span");

                //create repo url text
                let starText = document.createTextNode(`Start ${repo.stargazers_count}`);

                //Add STats count text to stars span
                StarsSpan.appendChild(starText);

                //Append Stars Count Span to  main div
                mainDive.appendChild(StarsSpan);

                //Add Class on mAin Div 
                mainDive.className = "repo-box";
                //Apend The Main Div To Conatiner
                reposData.appendChild(mainDive);

            });
        });

    }
}